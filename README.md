# アプリ名

MentaSupport

## 詳細

モバイルアプリ MentaSupport

## デモ

## 配色

##### Button, Border, TitleText

- ![#25E0E4](https://placehold.it/15/25E0E4/000000?text=+) `#25E0E4`
- ![#707070](https://placehold.it/15/707070/000000?text=+) `#707070`

##### Background

- linear-gradient(rgba(12, 19, 74, 1), #D22AD4)

##### Text

- ![#ffffff](https://placehold.it/15/ffffff/000000?text=+) `#ffffff`

##### TabBarBackground

- ![#021B1C](https://placehold.it/15/021B1C/000000?text=+) `#021B1C`

##### TabBarIcon

- ![#FD06FD](https://placehold.it/15/FD06FD/000000?text=+) `#FD06FD`

## 開発方法

特に決まり無し、

## CI

## 作者

[sibakeny](https://twitter.com/SibakenY)
