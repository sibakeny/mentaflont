import {createBottomTabNavigator} from 'react-navigation-tabs';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import HomeStack from './homeStack';
import {Icon} from 'react-native-elements';
import SearchMentarStack from './searchMentarStack';
import SearchConsultationStack from './searchConsultationStack';
import MessageStack from './messageStack';
import NotificationTabBarIcon from '../shared/NotificationTabBarIcon/index';

const screens = {
  HomeTab: {
    screen: HomeStack,
    navigationOptions: ({navigation}) => {
      return {
        tabBarLabel: 'ホーム',
        tabBarIcon: ({tintColor}) => (
          <Icon name="home" color={tintColor} size={24} />
        ),
      };
    },
  },
  SearchDirectSupport: {
    screen: SearchMentarStack,
    navigationOptions: ({navigation}) => {
      return {
        tabBarLabel: '相談相手',
        tabBarIcon: ({tintColor}) => (
          <Icon name="person" color={tintColor} size={24} />
        ),
      };
    },
  },
  SearchConsultation: {
    screen: SearchConsultationStack,
    navigationOptions: ({navigation}) => {
      return {
        tabBarLabel: '公開相談',
        tabBarIcon: ({tintColor}) => (
          <Icon name="book" color={tintColor} size={24} />
        ),
      };
    },
  },
  MessageBox: {
    screen: MessageStack,
    navigationOptions: ({navigation}) => {
      return {
        tabBarLabel: 'メッセージ',
        tabBarIcon: ({tintColor}) => (
          <View>
            <Icon name="mail" color={tintColor} size={24} />
            <NotificationTabBarIcon />
          </View>
        ),
      };
    },
  },
};

const HomeTab = createBottomTabNavigator(screens, {
  defaultNavigationOptions: ({navigation}) => {
    return {
      tabBarOptions: {
        style: {
          backgroundColor: '#021B1C',
        },
        activeTintColor: '#FD06FD',
      },
    };
  },
});

export default HomeTab;
