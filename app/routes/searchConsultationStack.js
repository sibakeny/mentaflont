import {createStackNavigator} from 'react-navigation-stack';
import {StyleSheet, View, Text, Image} from 'react-native';
import SearchConsultation from '../screens/HomeScreens/SearchConsultation';
import ConsultationDetail from '../screens/HomeScreens/ConsultationDetail';
import PostConsultationMessage from '../screens/HomeScreens/NewConsultationComment';

const screens = {
  SearchConsultation: {
    screen: SearchConsultation,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
  ConsultationDetail: {
    screen: ConsultationDetail,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
  PostConsultationMessage: {
    screen: PostConsultationMessage,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
};

const SearchMentarStack = createStackNavigator(screens, {
  defaultNavigationOptions: ({navigation}) => {
    return {
      header: null,
    };
  },
});

const styles = StyleSheet.create({
  icon: {
    left: 10,
  },
  headerBackground: {
    height: 80,
  },
});

export default SearchMentarStack;
