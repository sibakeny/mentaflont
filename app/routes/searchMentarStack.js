import {createStackNavigator} from 'react-navigation-stack';
import SearchDirectSupport from '../screens/HomeScreens/SearchMentar';
import MentarDetail from '../screens/HomeScreens/MentarDetail';
import PostMentarEvaluationComment from '../screens/HomeScreens/NewMentarEvaluationComment';
import EditMentar from '../screens/HomeScreens/EditMentar';

const screens = {
  SearchDirectSupport: {
    screen: SearchDirectSupport,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
  MentarDetail: {
    screen: MentarDetail,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
  PostMentarEvaluationComment: {
    screen: PostMentarEvaluationComment,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
  EditMentar: {
    screen: EditMentar,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
};

const SearchMentarStack = createStackNavigator(screens, {
  defaultNavigationOptions: ({navigation}) => {
    return {
      header: null,
    };
  },
});

export default SearchMentarStack;
