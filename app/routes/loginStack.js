import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import Login from '../screens/LoginScreens/Login';
import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import CreateUser from '../screens/LoginScreens/NewUser';

const screens = {
  Login: {
    screen: Login,
  },
  CreateUser: {
    screen: CreateUser,
  },
};

const SessionStack = createStackNavigator(screens, {
  defaultNavigationOptions: {
    header: null,
  },
});

const styles = StyleSheet.create({
  icon: {
    left: 10,
  },
  headerBackground: {
    height: 80,
  },
});

export default SessionStack;
