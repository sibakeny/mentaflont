import {createDrawerNavigator} from 'react-navigation-drawer';
import {createAppContainer} from 'react-navigation';
import HomeTab from './homeTab';
import AboutStack from './aboutStack';
import LoginStack from './loginStack';

const RootDrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: HomeTab,
      navigationOptions: {
        drawerLabel: 'ホーム',
      },
    },
    About: {
      screen: AboutStack,
      navigationOptions: {
        drawerLabel: 'アプリについて',
      },
    },
    Login: {
      screen: LoginStack,
      navigationOptions: {
        drawerLabel: () => null,
      },
    },
    Logout: {
      screen: LoginStack,
      navigationOptions: {
        drawerLabel: 'ログアウト',
      },
    },
  },
  {
    initialRouteName: 'Home',
    drawerBackgroundColor: 'rgba(2, 27, 28, 0.81)',
    contentOptions: {
      labelStyle: {color: 'white'},
    },
  },
);

export default createAppContainer(RootDrawerNavigator);
