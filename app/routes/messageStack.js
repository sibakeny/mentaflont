import {createStackNavigator} from 'react-navigation-stack';
import Header from '../shared/header';
import React from 'react';
import {StyleSheet} from 'react-native';
import DirectMessage from '../screens/HomeScreens/DirectMessage';
import MessageBox from '../screens/HomeScreens/MessageBox';

const screens = {
  MessageBox: {
    screen: MessageBox,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
  DirectMessage: {
    screen: DirectMessage,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
};

const SearchMentarStack = createStackNavigator(screens, {
  defaultNavigationOptions: ({navigation}) => {
    return {
      header: null,
    };
  },
});

const styles = StyleSheet.create({
  icon: {
    left: 10,
  },
  headerBackground: {
    height: 80,
  },
});

export default SearchMentarStack;
