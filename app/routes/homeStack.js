import {createStackNavigator} from 'react-navigation-stack';
import Home from '../screens/HomeScreens/Home';
import PostConsultation from '../screens/HomeScreens/NewConsultation';
import RegistarToMentar from '../screens/HomeScreens/RegisterToMentar';
import ConsultationHome from '../screens/HomeScreens/MentarHome';
import EditUserDescription from '../screens/HomeScreens/EditUser';
import EditMentar from '../screens/HomeScreens/EditMentar';
import UserConsultations from '../screens/HomeScreens/UserConsultations';
import UserConsultation from '../screens/HomeScreens/UserConsultation';
import NewConsultationComment from '../screens/HomeScreens/NewConsultationComment';
import PostMentarEvaluationComment from '../screens/HomeScreens/NewMentarEvaluationComment';

const screens = {
  HomeScreen: {
    screen: Home,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
  PostConsultation: {
    screen: PostConsultation,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
  RegistarToMentar: {
    screen: RegistarToMentar,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
  ConsultationHome: {
    screen: ConsultationHome,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
  EditUserDescription: {
    screen: EditUserDescription,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },

  EditMentar: {
    screen: EditMentar,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
  UserConsultations: {
    screen: UserConsultations,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
  UserConsultation: {
    screen: UserConsultation,
    navigationOptions: ({}) => {
      return {};
    },
  },
  PostConsultationMessage: {
    screen: NewConsultationComment,
    navigationOptions: ({}) => {
      return {};
    },
  },
  PostMentarEvaluationComment: {
    screen: PostMentarEvaluationComment,
    navigationOptions: ({navigation}) => {
      return {};
    },
  },
};

const HomeStack = createStackNavigator(screens, {
  defaultNavigationOptions: ({navigation}) => {
    return {
      header: null,
      headerStyle: {
        backgroundColor: 'transparent',
        zIndex: 100,
        top: 0,
        left: 0,
        right: 0,
        borderBottomWidth: 0,
      },
    };
  },
});

export default HomeStack;
