import {createStackNavigator} from 'react-navigation-stack';
import About from '../screens/AboutScreens/About';
import Header from '../shared/header';
import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const screens = {
  About: {
    screen: About,
    navigationOptions: ({navigation}) => {
      return {
        headerTitle: () => <Header title={'MentaSupport'} />,
        headerLeft: () => (
          <TouchableOpacity style={{marginLeft: 15}}>
            <Icon
              name={'list'}
              size={20}
              onPress={() => navigation.openDrawer()}
            />
          </TouchableOpacity>
        ),
      };
    },
  },
};

const AboutStack = createStackNavigator(screens, {
  defaultNavigationOptions: {
    headerTintColor: '#444',
    headerStyle: {backgroundColor: '#eee', height: 80},
    headerLeft: <View style={{padding: 6}}></View>,
    headerTitleAlign: 'center',
  },
});

const styles = StyleSheet.create({
  icon: {
    left: 10,
  },
});

export default AboutStack;
