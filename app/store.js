import {persistReducer, persistStore} from 'redux-persist';
import reducers from './reducers/reducers';
import {createStore, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk';
import AsyncStorage from '@react-native-community/async-storage';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};
const persistedReducer = persistReducer(persistConfig, reducers);

let store = createStore(persistedReducer, applyMiddleware(ReduxThunk));

export const persistor = persistStore(store);

export default store;
