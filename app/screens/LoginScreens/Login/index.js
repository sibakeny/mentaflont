import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, TouchableOpacity, Text, TextInput} from 'react-native';
import {bindActionCreators} from 'redux';
import * as Actions from '../../../actions';
import LinearGradient from 'react-native-linear-gradient';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null,
    };
  }

  componentDidMount() {
    this.props.logout();
  }

  componentDidUpdate() {
    if (this.props.auth.token != '') {
      this.props.navigation.navigate('HomeScreen');
    }
  }

  onButtonPress() {
    const {email, password} = this.state;
    this.props.submitLogin({email, password});
  }

  navigateToCreateUser = () => {
    this.props.navigation.navigate('CreateUser', {});
  };

  render() {
    return (
      <LinearGradient
        style={{flex: 1}}
        colors={['rgba(12, 19, 74, 1)', '#D22AD4']}
        start={{x: 1, y: 0}}
        end={{x: 1, y: 1}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View style={styles.loginWrapper}>
            <View style={styles.loginContainer}>
              <View>
                <View style={styles.textWrap}>
                  <Text style={{color: 'white'}}>ログイン</Text>
                </View>
                <View style={styles.wrap}>
                  <TextInput
                    placeholder="メールアドレス"
                    autoCorrect={false}
                    value={this.state.email}
                    onChangeText={email => this.setState({email: email})}
                    style={styles.inputStyle}
                    placeholderTextColor={'gray'}
                  />
                </View>
                <View style={styles.wrap}>
                  <TextInput
                    secureTextEntry
                    placeholder="パスワード"
                    autoCorrect={false}
                    value={this.state.password}
                    onChangeText={password =>
                      this.setState({password: password})
                    }
                    style={styles.inputStyle}
                    placeholderTextColor={'gray'}
                  />
                </View>
                <View style={styles.createUserTextWrapper}>
                  <Text style={{color: 'white'}}>アカウントがない場合は</Text>

                  <Text
                    onPress={this.navigateToCreateUser}
                    style={styles.createUserBtn}>
                    こちら
                  </Text>
                </View>

                <View style={styles.ButtonWrap}>
                  <TouchableOpacity
                    onPress={this.onButtonPress.bind(this)}
                    style={styles.buttonStyle}>
                    <Text style={styles.textStyle}>ログイン</Text>
                  </TouchableOpacity>
                </View>

                {/* <Text>
                    {this.props.loggedIn}
                    {firebase.auth().currentUser && firebase.auth().currentUser.email}
                    </Text> */}
              </View>
            </View>
          </View>
        </View>
      </LinearGradient>
    );
  }
}

const styles = {
  wrap: {
    padding: 10,
  },
  ButtonWrap: {
    padding: 10,
    flex: 1,
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  textWrap: {
    padding: 10,
    marginBottom: 30,
    flex: 1,
    alignItems: 'center',
  },
  textStyle: {
    color: 'white',
    fontSize: 16,
    fontWeight: '600',
    top: 5,
  },
  buttonStyle: {
    backgroundColor: '#14B7FF',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#007aff',
    width: 100,
    height: 35,
    alignItems: 'center',
  },
  inputStyle: {
    color: 'white',
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 18,
    lineHeight: 23,
    height: 40,
    borderWidth: 1,
    borderColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 10,
  },
  loginContainer: {
    width: '100%',
    marginHorizontal: 4,
    marginVertical: 6,
    paddingHorizontal: 10,
    paddingTop: 20,
  },
  loginWrapper: {
    height: 350,
    backgroundColor: "backgroundColor: 'rgba(39, 21, 110, 0.1)'",
    borderWidth: 0,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.46,
    shadowRadius: 11.14,
    width: '90%',
    elevation: 5,
  },
  createUserTextWrapper: {
    width: '90%',
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 20,
  },
  createUserBtn: {
    color: '#14B7FF',
  },
};

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
