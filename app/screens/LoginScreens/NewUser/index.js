import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, TouchableOpacity, Text, TextInput} from 'react-native';
import {bindActionCreators} from 'redux';
import * as Actions from '../../../actions';
import axios from 'axios';
import {url} from '../../../shared/const';
import LinearGradient from 'react-native-linear-gradient';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {email: '', password: '', name: ''};
  }

  componentWillMount() {}

  componentDidUpdate() {}

  componentDidMount() {}

  async onButtonPress() {
    var returnFlag = false;

    axios
      .post(url + '/users', {
        email: this.state.email,
        password: this.state.password,
        name: this.state.name,
      })
      .then(res => {
        this.props.navigation.navigate('Login', {});
      });
  }

  render() {
    return (
      <LinearGradient
        style={{flex: 1}}
        colors={['rgba(12, 19, 74, 1)', '#D22AD4']}
        start={{x: 1, y: 0}}
        end={{x: 1, y: 1}}>
        <View style={styles.container}>
          <View style={styles.wrapAll}>
            <View style={styles.consultationTitle}>
              <Text style={styles.consultationTitleText}>アカウント作成</Text>
            </View>
            <View style={styles.postCard}>
              <View style={styles.postCardWrap}>
                <View style={styles.inputBoxGroup}>
                  <Text style={{color: 'white'}}>名前</Text>
                  <TextInput
                    onChangeText={val => {
                      this.setState({name: val});
                    }}
                    style={styles.searchTextInput}
                  />
                </View>
                <View style={styles.inputBoxGroup}>
                  <Text style={{color: 'white'}}>メールアドレス</Text>
                  <TextInput
                    onChangeText={val => {
                      this.setState({email: val});
                    }}
                    style={styles.searchTextInput}
                  />
                </View>
                <View style={styles.inputBoxGroup}>
                  <Text style={{color: 'white'}}>パスワード</Text>
                  <TextInput
                    onChangeText={val => {
                      this.setState({password: val});
                    }}
                    style={styles.searchTextInput}
                  />
                </View>
                <View style={styles.submitButtonGroup}>
                  <TouchableOpacity
                    style={styles.submitButton}
                    onPress={this.onButtonPress.bind(this)}>
                    <Text style={styles.submitButtonText}>作成</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
      </LinearGradient>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    top: -30,
  },
  wrapAll: {
    width: '90%',
  },
  postCard: {
    paddingBottom: 40,
    paddingTop: 20,
    paddingHorizontal: 10,
    marginTop: 10,
    backgroundColor: "backgroundColor: 'rgba(39, 21, 110, 0.1)'",
    borderWidth: 0,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.46,
    shadowRadius: 11.14,

    elevation: 5,
  },
  consultationTitle: {
    marginTop: 20,
  },
  consultationTitleText: {
    fontSize: 20,
    color: 'white',
  },
  postCardWrap: {
    paddingHorizontal: 10,
    paddingVertical: 4,
  },
  selectBoxGroup: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 30,
    borderBottomWidth: 1,
    borderColor: '#707070',
    paddingVertical: 5,
    marginVertical: 5,
  },
  picker: {
    width: 150,
    borderWidth: 1,
    borderColor: '#707070',
  },
  inputBoxGroup: {
    marginVertical: 5,
  },
  searchTextInput: {
    height: 40,
    borderWidth: 1,
    borderColor: '#707070',
    marginTop: 10,
    padding: 4,
    color: 'white',
  },
  searchTextArea: {
    height: 200,
    borderWidth: 1,
    borderColor: '#707070',
    marginTop: 10,
    padding: 4,
    textAlignVertical: 'top',
  },
  submitButtonGroup: {
    flex: 1,
    alignItems: 'center',
    marginVertical: 20,
  },
  submitButton: {
    backgroundColor: '#14B7FF',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#007aff',
    width: 100,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
  },
  submitButtonText: {
    color: 'white',
  },
};

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
