import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, TouchableOpacity, Text, ScrollView, Image} from 'react-native';
import {bindActionCreators} from 'redux';
import * as Actions from '../../../actions';
import axios from 'axios';
import {url} from '../../../shared/const';
import styles from './styles.js';
import Header from '../../../shared/header';
import LinearGradient from 'react-native-linear-gradient';
import ConsultationCard from './ConsultationCard';
import CommentCard from '../../../shared/CommentCard';

class UserConsultation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      consultation: null,
      comments: [],
      loading: false,
      stopLoading: false,
      page: 0,
    };
  }
  componentDidMount() {
    const consultation_id = this.props.navigation.state.params.consultation_id;
    axios.get(url + '/consultations/' + consultation_id).then(res => {
      this.setState({consultation: res.data.data});
    });

    axios
      .get(url + '/comments', {
        params: {
          target_id: consultation_id,
          target_type: 'Consultation',
        },
      })
      .then(res => {
        this.setState({comments: res.data.data});
      });

    this.subs = [
      this.props.navigation.addListener('didFocus', () => this._onFocus()),
    ];
  }

  _onFocus = () => {
    const consultation_id = this.props.navigation.state.params.consultation_id;
    axios.get(url + '/consultations/' + consultation_id).then(res => {
      this.setState({consultation: res.data.data});
    });

    axios
      .get(url + '/comments', {
        params: {
          target_id: consultation_id,
          target_type: 'Consultation',
        },
      })
      .then(res => {
        this.setState({comments: res.data.data});
      });
  };

  componentWillUnmount() {
    this.subs.forEach(sub => sub.remove());
  }

  goDirectMessage = () => {
    this.props.navigation.navigate('DirectMessage');
  };

  navigateToPostConsultationMessage = () => {
    this.props.navigation.navigate('PostConsultationMessage', {
      consultation: this.state.consultation,
    });
  };

  loadMoreItems() {
    axios
      .get(url + '/comments', {
        params: {
          target_id: this.props.navigation.state.params.consultation_id,
          target_type: 'Consultation',
          page: this.state.page + 1,
        },
      })
      .then(res => {
        var comments = this.state.comments;
        this.setState({page: this.state.page + 1});
        this.setState({loading: false});

        if (res.data.data.length > 0) {
          this.setState({stopLoading: false});
          this.setState({comments: comments.concat(res.data.data)});
        } else {
          this.setState({stopLoading: true});
        }
      });
  }

  isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  comments = () => {
    return this.state.comments.map(comment => {
      return <CommentCard comment={comment} />;
    });
  };

  statusChange = status => {
    axios.patch(
      url +
        '/consultations/' +
        this.props.navigation.state.params.consultation_id +
        '/status',
      {
        status: status,
      },
    );

    axios
      .get(
        url +
          '/consultations/' +
          this.props.navigation.state.params.consultation_id,
      )
      .then(res => {
        this.setState({consultation: res.data.data});
      });
  };

  render() {
    return (
      <LinearGradient
        style={{flex: 1}}
        colors={['rgba(12, 19, 74, 1)', '#D22AD4']}
        start={{x: 1, y: 0}}
        end={{x: 1, y: 1}}>
        <Header
          navigation={this.props.navigation}
          title={'MentarSupport'}
          child={true}
        />
        <ScrollView
          onScroll={({nativeEvent}) => {
            if (this.isCloseToBottom(nativeEvent)) {
              if (!this.state.loading && !this.state.stopLoading) {
                this.setState({loading: true});
                this.loadMoreItems();
              }
            }
          }}
          scrollEventThrottle={400}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-end',
              marginRight: 15,
            }}>
            <TouchableOpacity
              onPress={() => this.statusChange('open')}
              style={{
                paddingHorizontal: 12,
                paddingVertical: 4,
                borderWidth: 1,
                borderColor: '#53C519',
                marginRight: 15,
              }}>
              <Text style={{color: 'white'}}>公開</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.statusChange('resolved')}
              style={{
                paddingHorizontal: 12,
                paddingVertical: 4,
                borderWidth: 1,
                borderColor: '#2189D9',
                marginRight: 15,
              }}>
              <Text style={{color: 'white'}}>解決</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.statusChange('pending')}
              style={{
                paddingHorizontal: 12,
                paddingVertical: 4,
                borderColor: 'orange',
                borderWidth: 1,
                marginRight: 15,
              }}>
              <Text style={{color: 'white'}}>一時停止</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.statusChange('close')}
              style={{
                paddingHorizontal: 12,
                paddingVertical: 4,
                borderWidth: 1,
                borderColor: 'red',
              }}>
              <Text style={{color: 'white'}}>終了</Text>
            </TouchableOpacity>
          </View>
          <View>
            <ConsultationCard consultation={this.state.consultation} />
          </View>
          <View style={styles.ButtonContainer}>
            <TouchableOpacity
              style={styles.sendButton}
              onPress={this.navigateToPostConsultationMessage}>
              <Text style={styles.sendButtonText}>コメントの投稿</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.mentarsTitle}>
            <Text style={styles.mentarsTitleText}>コメント</Text>
          </View>
          <View style={{marginHorizontal: 10}}>{this.comments()}</View>
        </ScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(UserConsultation);
