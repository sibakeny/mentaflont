import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Avatar, Badge} from 'react-native-elements';
import styles from './styles.js';

const ConsultationCard = props => {
  const mentarCategoryBadge = category => {
    if (category != '') {
      return <Badge value={category} status="success" />;
    }
  };

  const mentarSexBadge = sex => {
    if (sex != '') {
      if (sex == '1') {
        return <Badge value={'男性'} status="success" />;
      } else {
        return <Badge value={'女性'} status="success" />;
      }
    }
  };

  const statusBadge = () => {
    if (props.consultation.attributes.status == 'open') {
      return (
        <View style={styles.openBadgeStyle}>
          <Text style={{color: 'white'}}>公開中</Text>
        </View>
      );
    } else if (props.consultation.attributes.status == 'pending') {
      return (
        <View style={styles.pendingBadgeStyle}>
          <Text style={{color: 'white'}}>一時停止中</Text>
        </View>
      );
    } else if (props.consultation.attributes.status == 'close') {
      return (
        <View style={styles.closeBadgeStyle}>
          <Text style={{color: 'white'}}>終了</Text>
        </View>
      );
    } else if (props.consultation.attributes.status == 'resolved') {
      return (
        <View style={styles.resolvedBadgeStyle}>
          <Text style={{color: 'white'}}>解決</Text>
        </View>
      );
    }
  };

  if (props.consultation && props.consultation.attributes) {
    return (
      <View style={styles.mentarCard} key={props.consultation.attributes.id}>
        <View style={styles.mentarCardWrapper}>
          <View style={styles.userInfoHeader}>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                flex: 1,
              }}>
              <Text style={styles.userName}>
                {props.consultation.attributes.title}
              </Text>
            </View>
            {statusBadge()}
          </View>

          <View style={styles.userInfoBody}>
            <Text style={styles.textWhite}>
              {props.consultation.attributes.description}
            </Text>
          </View>
          <View style={styles.badgeTopContainer}>
            <View style={styles.badgeContainer}>
              <View style={{marginRight: 5}}>
                {mentarCategoryBadge(props.consultation.attributes.category)}
              </View>
              <View>{mentarSexBadge(props.consultation.attributes.sex)}</View>
            </View>
            <View style={styles.userNameContainer}>
              <Avatar
                containerStyle={styles.avatarStyle}
                rounded
                source={{
                  uri: props.consultation.attributes.image_url,
                }}
                size="small"
              />
              <View>
                <Text style={styles.userInfoName}>
                  {props.consultation.attributes.user_name}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  } else {
    return null;
  }
};

export default ConsultationCard;
