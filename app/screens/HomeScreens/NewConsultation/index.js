import React from 'react';
import {connect} from 'react-redux';
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Picker,
  TouchableOpacity,
} from 'react-native';
import {bindActionCreators} from 'redux';
import * as Actions from '../../../actions';
import axios from 'axios';
import {url} from '../../../shared/const';
import styles from './styles';
import Header from '../../../shared/header';
import LinearGradient from 'react-native-linear-gradient';
import {NavigationActions, StackActions} from 'react-navigation';

class PostConsultation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      category: '',
      sex: '',
      title: '',
      description: '',
    };
  }

  componentDidMount() {}

  navigtateToConsultationDetail = () => {
    this.props.navigation.navigate('ConsultationDetail', {});
  };

  handleSubmit = () => {
    axios
      .post(url + '/consultations', {
        category: this.state.category,
        title: this.state.title,
        sex: this.state.sex,
        description: this.state.description,
        user_id: this.props.user.currentUser.id,
      })
      .then(res => {
        this.props.navigation.dispatch(StackActions.popToTop());
        this.props.navigation.navigate('UserConsultation', {
          consultation_id: res.data.data.attributes.id,
        });
      });
  };

  render() {
    return (
      <LinearGradient
        style={{flex: 1}}
        colors={['rgba(12, 19, 74, 1)', '#D22AD4']}
        start={{x: 1, y: 0}}
        end={{x: 1, y: 1}}>
        <Header navigation={this.props.navigation} child={true} />
        <ScrollView>
          <View style={styles.container}>
            <View style={styles.wrapAll}>
              <View style={styles.consultationTitle}>
                <Text style={styles.consultationTitleText}>相談の投稿</Text>
              </View>
              <View
                style={{
                  paddingBottom: 10,
                  paddingTop: 10,
                  paddingHorizontal: 4,
                  margin: 0,
                  backgroundColor: 'transparent',
                  borderWidth: 0,
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 8,
                    height: 8,
                  },
                  shadowOpacity: 0.46,
                  shadowRadius: 11.14,

                  elevation: 5,
                }}>
                <View style={styles.postCardWrap}>
                  <View style={styles.selectBoxGroup}>
                    <Text style={{color: 'white'}}>カテゴリ</Text>
                    <Picker
                      style={styles.picker}
                      onValueChange={itemValue =>
                        this.setState({category: itemValue})
                      }
                      selectedValue={this.state.category}>
                      <Picker.Item value="" label="指定なし" />
                      <Picker.Item value="仕事" label="仕事" />
                      <Picker.Item value="趣味" label="趣味" />
                      <Picker.Item value="お金" label="お金" />
                      <Picker.Item value="勉強" label="勉強" />
                    </Picker>
                  </View>
                  <View style={styles.selectBoxGroup}>
                    <Text style={{color: 'white'}}>性別</Text>
                    <Picker
                      style={styles.picker}
                      onValueChange={itemValue =>
                        this.setState({sex: itemValue})
                      }
                      selectedValue={this.state.sex}>
                      <Picker.Item
                        style={{color: 'white'}}
                        value="0"
                        label="指定なし"
                      />
                      <Picker.Item
                        style={{color: 'white'}}
                        value="1"
                        label="男"
                      />
                      <Picker.Item
                        style={{color: 'white'}}
                        value="2"
                        label="女"
                      />
                    </Picker>
                  </View>
                  <View style={styles.inputBoxGroup}>
                    <Text style={{color: 'white'}}>タイトル</Text>
                    <TextInput
                      style={styles.searchTextInput}
                      onChangeText={val => this.setState({title: val})}
                    />
                  </View>
                  <View style={styles.inputBoxGroup}>
                    <Text style={{color: 'white'}}>本文</Text>
                    <TextInput
                      multiline={true}
                      style={styles.searchTextArea}
                      onChangeText={val => this.setState({description: val})}
                    />
                  </View>
                  <View style={styles.submitButtonGroup}>
                    <TouchableOpacity
                      style={styles.submitButton}
                      onPress={this.handleSubmit}>
                      <Text style={styles.submitButtonText}>投稿</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(PostConsultation);
