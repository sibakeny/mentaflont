import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
  },
  wrapAll: {
    width: '90%',
  },
  postCard: {
    marginTop: 20,
    borderRadius: 6,
    elevation: 3,
    backgroundColor: '#fff',
    shadowOffset: {width: 1, height: 1},
    shadowColor: '#333',
    shadowOpacity: 0.3,
    shadowRadius: 2,
    marginBottom: 20,
  },
  consultationTitle: {
    marginTop: 0,
    marginBottom: 10,
  },
  consultationTitleText: {
    fontSize: 20,
    color: 'white',
  },
  postCardWrap: {
    paddingHorizontal: 10,
    paddingVertical: 4,
  },
  selectBoxGroup: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 30,
    borderBottomWidth: 1,
    borderColor: '#707070',
    paddingVertical: 5,
    marginVertical: 5,
  },
  picker: {
    width: 150,
    borderWidth: 1,
    borderColor: '#707070',
    color: 'white',
  },
  inputBoxGroup: {
    marginVertical: 5,
  },
  searchTextInput: {
    height: 40,
    borderWidth: 1,
    borderColor: '#707070',
    marginTop: 10,
    padding: 4,
    color: 'white',
  },
  searchTextArea: {
    height: 200,
    borderWidth: 1,
    borderColor: '#707070',
    marginTop: 10,
    padding: 4,
    textAlignVertical: 'top',
    color: 'white',
  },
  submitButtonGroup: {
    flex: 1,
    alignItems: 'center',
    marginVertical: 20,
  },
  submitButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 30,
    width: 80,
    borderRadius: 6,
    elevation: 3,
    backgroundColor: '#25E0E4',
    shadowOffset: {width: 1, height: 1},
    shadowColor: '#333',
    shadowOpacity: 0.3,
    shadowRadius: 2,
  },
  submitButtonText: {
    color: 'white',
  },
});

export default styles;
