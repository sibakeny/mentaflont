import React from 'react';
import {connect} from 'react-redux';
import {View, Text, Keyboard} from 'react-native';
import {bindActionCreators} from 'redux';
import * as Actions from '../../../actions';
import {GiftedChat} from 'react-native-gifted-chat';
import {TouchableOpacity} from 'react-native-gesture-handler';
import axios from 'axios';
import {url} from '../../../shared/const';
import Pusher from 'pusher-js/react-native';
import {PUSHER_API_KEY} from 'react-native-dotenv';
import styles from './styles.js';
import LinearGradient from 'react-native-linear-gradient';
import Header from '../../../shared/header';

var isMounted = false;

class DirectMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      message: '',
      reciever: null,
      page: 1,
      loading: false,
      stopLoading: false,
    };
    this.giftedChatRef = React.createRef();
  }

  async componentDidMount() {
    isMounted = true;

    await axios.delete(url + '/notifications/all', {
      params: {
        token: this.props.auth.token,
        category: 'directMessage',
        sender_id: this.props.navigation.state.params.reciever_id,
      },
    });

    await axios
      .get(
        url +
          '/users/with_id/' +
          this.props.navigation.state.params.reciever_id,
      )
      .then(res => {
        if (isMounted) {
          this.setState({reciever: res.data});
        }
      });

    await axios
      .get(url + '/direct_message_comment', {
        params: {
          sender_id: this.props.user.currentUser.id,
          reciever_id: this.props.navigation.state.params.reciever_id,
          target_type: null,
        },
      })
      .then(res => {
        var messages = [];
        res.data.data.map(message => {
          messages.push({
            _id: message.attributes.id,
            text: message.attributes.body,
            createdAt: message.attributes.created_at,
            user: {
              _id: parseInt(message.relationships.sender.data.id, 10),
              avatar: message.attributes.image_url,
            },
          });
        });

        this.setState({messages: messages});
      });

    var pusher = new Pusher(PUSHER_API_KEY, {
      cluster: 'ap3',
      forceTLS: true,
    });

    var channel = pusher.subscribe(this.room_id());
    var self = this;
    channel.bind('my-event', function(data) {
      self.setState({stopLoading: false});
      var messages = Object.assign([], self.state.messages);
      messages.unshift({
        _id: data.id,
        text: data.body,
        createdAt: data.createdAt,
        user: {
          _id: parseInt(data.sender.id, 10),
        },
      });
      self.setState({messages: messages});
    });

    this.giftedChatRef.scrollToBottom();
  }

  componentWillUnmount() {
    isMounted = false;
  }

  room_id = () => {
    const reciever_id = this.props.navigation.state.params.reciever_id;

    const user_id = this.props.user.currentUser.id;

    var room_id = '';

    if (reciever_id > user_id) {
      room_id = user_id + '_' + reciever_id;
    } else {
      room_id = reciever_id + '_' + user_id;
    }

    return room_id;
  };

  onSend() {
    Keyboard.dismiss();
    axios.post(url + '/direct_message_comment', {
      body: this.state.message,
      sender_id: this.props.user.currentUser.id,
      reciever_id: this.props.navigation.state.params.reciever_id,
      category: 'directMessage',
    });
  }

  sendButton = props => {
    return (
      <TouchableOpacity
        style={styles.sendButton}
        onPress={() => props.onSend({text: props.text}, true)}>
        <View style={styles.sendButtonWrap}>
          <Text style={styles.buttonText}>送信</Text>
        </View>
      </TouchableOpacity>
    );
  };

  isCloseToTop({layoutMeasurement, contentOffset, contentSize}) {
    const paddingToTop = 80;
    return (
      contentSize.height - layoutMeasurement.height - paddingToTop <=
      contentOffset.y
    );
  }

  loadMoreChat = async () => {
    this.setState({stopLoading: true});
    await axios
      .get(url + '/direct_message_comment', {
        params: {
          sender_id: this.props.user.currentUser.id,
          reciever_id: this.props.navigation.state.params.reciever_id,
          target_type: null,
          page: this.state.page,
        },
      })
      .then(res => {
        this.setState({loading: false});
        var messages = [];

        res.data.data.map(message => {
          messages.push({
            _id: message.attributes.id,
            text: message.attributes.body,
            createdAt: message.attributes.created_at,
            user: {
              _id: parseInt(message.relationships.sender.data.id, 10),
              avatar: message.attributes.image_url,
            },
          });
        });

        if (messages.length > 0) {
          var currentMessage = this.state.messages;
          this.setState({messages: currentMessage.concat(messages)});
          this.setState({stopLoading: false});
        }
      });
  };

  render() {
    return (
      <LinearGradient
        style={{flex: 1}}
        colors={['rgba(12, 19, 74, 1)', '#D22AD4']}
        start={{x: 1, y: 0}}
        end={{x: 1, y: 1}}>
        <Header
          navigation={this.props.navigation}
          title={'MentarSupport'}
          child={true}
        />
        <GiftedChat
          keyboardShouldPersistTaps={'handled'}
          messages={this.state.messages}
          onSend={() => this.onSend()}
          user={{
            _id:
              this.props.user.currentUser &&
              parseInt(this.props.user.currentUser.id, 10),
          }}
          renderSend={this.sendButton}
          placeholder={'テキストを入力してください'}
          onInputTextChanged={message => this.setState({message: message})}
          inverted={true}
          showAvatarForEveryMessage={true}
          scrollToBottom={true}
          ref={ref => (this.giftedChatRef = ref)}
          listViewProps={{
            scrollEventThrottle: 400,
            onScroll: ({nativeEvent}) => {
              if (this.isCloseToTop(nativeEvent)) {
                if (!this.state.loading && !this.state.stopLoading) {
                  this.setState({page: this.state.page + 1});
                  this.setState({loading: true});
                  this.loadMoreChat();
                }
              }
            },
          }}
        />
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(DirectMessage);
