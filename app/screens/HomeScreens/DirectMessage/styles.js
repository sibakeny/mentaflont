import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  sendButton: {
    width: 60,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  sendButtonWrap: {
    height: '100%',
    width: 60,
    backgroundColor: '#14B7FF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: 'white',
  },
});

export default styles;
