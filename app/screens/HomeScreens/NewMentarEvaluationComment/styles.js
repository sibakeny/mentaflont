import {StyleSheet} from 'react-native';

const styles = {
  container: {
    alignItems: 'center',
    flex: 1,
  },
  titleWrap: {
    marginVertical: 20,
  },
  title: {
    fontSize: 20,
    color: 'white',
  },
  textAreaWrap: {
    width: '90%',
  },
  textArea: {
    height: 200,
    borderWidth: 1,
    borderColor: '#707070',
    backgroundColor: 'transparent',
    marginTop: 10,
    padding: 4,
    textAlignVertical: 'top',
    color: 'white',
  },
  submitButtonGroup: {
    flex: 1,
    alignItems: 'center',
    marginVertical: 40,
  },
  submitButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 30,
    width: 80,
    borderRadius: 6,
    elevation: 3,
    backgroundColor: '#14B7FF',
    shadowOffset: {width: 1, height: 1},
    shadowColor: '#333',
    shadowOpacity: 0.3,
    shadowRadius: 2,
  },
  submitButtonText: {
    color: 'white',
  },
};

export default styles;
