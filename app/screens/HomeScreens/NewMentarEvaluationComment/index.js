import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  View,
  TouchableOpacity,
  Text,
  TextInput,
  ScrollView,
} from 'react-native';
import {bindActionCreators} from 'redux';
import * as Actions from '../../../actions';
import axios from 'axios';
import {url} from '../../../shared/const';
import styles from './styles';
import Header from '../../../shared/header';
import LinearGradient from 'react-native-linear-gradient';

class PostMentarEvaluationComment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mentar: null,
      comments: [],
      comment: '',
    };
  }

  componentDidMount() {}

  goBackToPostConsultation = () => {
    this.props.navigation.goBack();
  };

  handleSubmit = () => {
    axios
      .post(url + '/comments', {
        sender_id: this.props.user.currentUser.id,
        reciever_id: this.props.navigation.state.params.mentar.attributes.id,
        body: this.state.comment,
        target_type: 'Mentar',
        target_id: this.props.navigation.state.params.mentar.attributes.id,
        token: this.props.auth.token,
      })
      .then(() => {
        this.props.navigation.goBack();
      });
  };

  render() {
    return (
      <LinearGradient
        style={{flex: 1}}
        colors={['rgba(12, 19, 74, 1)', '#D22AD4']}
        start={{x: 1, y: 0}}
        end={{x: 1, y: 1}}>
        <Header
          navigation={this.props.navigation}
          title={'MentarSupport'}
          child={true}
        />
        <ScrollView>
          <View style={styles.container}>
            <View style={styles.titleWrap}>
              <Text style={styles.title}>コメントの投稿</Text>
            </View>
            <View style={styles.textAreaWrap}>
              <TextInput
                multiline={true}
                style={styles.textArea}
                onChangeText={val => this.setState({comment: val})}
              />
            </View>
            <View style={styles.submitButtonGroup}>
              <TouchableOpacity
                style={styles.submitButton}
                onPress={this.handleSubmit}>
                <Text style={styles.submitButtonText}>投稿</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PostMentarEvaluationComment);
