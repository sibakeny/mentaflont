import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  View,
  TouchableOpacity,
  Text,
  TextInput,
  ScrollView,
  Image,
} from 'react-native';
import {bindActionCreators} from 'redux';
import * as Actions from '../../../actions';
import axios from 'axios';
import {url} from '../../../shared/const';
import ImagePicker from 'react-native-image-picker';
import styles from './styles.js';
import Header from '../../../shared/header';
import LinearGradient from 'react-native-linear-gradient';
import ImageResizer from 'react-native-image-resizer';

class EditUser extends Component {
  constructor(props) {
    super(props);
    this.state = {description: '', name: '', image: null, currentUser: null};
  }

  componentDidMount = async () => {
    await axios
      .get(url + '/users/current_user', {
        params: {
          token: this.props.auth.token,
        },
      })
      .then(res => {
        this.setState({description: res.data.data.attributes.description});
        this.setState({name: res.data.data.attributes.name});
        this.setState({currentUser: res.data.data});
      });

    this.subs = [
      this.props.navigation.addListener('didFocus', () => this._onFocus()),
    ];
  };

  componentWillUnmount() {
    this.subs.forEach(sub => sub.remove());
  }

  _onFocus = () => {
    axios
      .get(url + '/users/current_user', {
        params: {
          token: this.props.auth.token,
        },
      })
      .then(res => {
        this.setState({description: res.data.data.attributes.description});
        this.setState({name: res.data.data.attributes.name});
        this.setState({currentUser: res.data.data});
        console.log(this.state.currentUser);
      });
  };

  goBackToPostConsultation = () => {
    this.props.navigation.goBack();
  };
  handleSubmit = () => {
    console.log(this.state.currentUser.attributes);
    const formData = new FormData();
    formData.append('user[name]', this.state.name);
    formData.append('user[description]', this.state.description);
    formData.append('user[email]', this.state.currentUser.attributes.email);
    if (this.state.image && this.state.image.uri)
      formData.append('user[image]', {
        uri: this.state.image.uri,
        name: `avatar.jpg`,
        type: `image/jpg`,
      });
    axios({
      method: 'PATCH',
      url: url + '/users/' + this.props.auth.token,
      dataType: 'JSON',
      contentType: false,
      processData: false,
      data: formData,
    }).then(() => {
      this.props.navigation.goBack();
    });
  };

  _pickImage = () => {
    const options = {
      title: 'Select Avatar',
      customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        ImageResizer.createResizedImage(response.uri, 200, 200, 'JPEG', 100)
          .then(({uri}) => {
            this.setState({
              image: {uri: uri},
            });
          })
          .catch(err => {
            console.log(err);
          });
      }
    });
  };

  render() {
    return (
      <LinearGradient
        style={{flex: 1}}
        colors={['rgba(12, 19, 74, 1)', '#D22AD4']}
        start={{x: 1, y: 0}}
        end={{x: 1, y: 1}}>
        <Header
          navigation={this.props.navigation}
          title={'MentarSupport'}
          child={true}
        />
        <ScrollView>
          <View style={styles.container}>
            <View style={styles.wrapAll}>
              <View style={styles.consultationTitle}>
                <Text style={styles.consultationTitleText}>
                  ユーザー情報編集
                </Text>
              </View>
              <View style={styles.postCard}>
                <View style={styles.postCardWrap}>
                  <View style={styles.inputBoxGroup}>
                    <View style={styles.userImageGroup}>
                      <Image
                        source={{
                          uri:
                            (this.state.image && this.state.image.uri) ||
                            (this.state.currentUser &&
                              this.state.currentUser.image_url),
                        }}
                        style={styles.userImage}
                      />
                      <TouchableOpacity
                        style={styles.imageButton}
                        onPress={this._pickImage}>
                        <Text style={styles.submitButtonText}>画像</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={styles.inputBoxGroup}>
                    <Text style={{color: 'white'}}>名前</Text>
                    <TextInput
                      onChangeText={val => {
                        this.setState({name: val});
                      }}
                      value={this.state.name}
                      style={styles.searchTextInput}
                    />
                  </View>
                  <View style={styles.inputBoxGroup}>
                    <Text style={{color: 'white'}}>本文</Text>
                    <TextInput
                      multiline={true}
                      style={styles.searchTextArea}
                      onChangeText={val => this.setState({description: val})}
                      value={this.state.description}
                    />
                  </View>
                  <View style={styles.submitButtonGroup}>
                    <TouchableOpacity
                      style={styles.submitButton}
                      onPress={this.handleSubmit.bind(this)}>
                      <Text style={styles.submitButtonText}>登録</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(EditUser);
