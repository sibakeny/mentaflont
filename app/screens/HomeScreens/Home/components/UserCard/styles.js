const styles = {
  userCardContainer: {
    paddingBottom: 20,
    paddingTop: 10,
    margin: 0,
    backgroundColor: "backgroundColor: 'rgba(39, 21, 110, 0.1)'",
    borderWidth: 0,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.46,
    shadowRadius: 11.14,

    elevation: 5,
  },
  userInfoHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 70,
    position: 'relative',
    width: '90%',
    paddingVertical: 10,
  },
  userInfoHeaderContainer: {
    alignItems: 'center',
    paddingVertical: 10,
  },
  userImage: {},
  userName: {
    width: '80%',
    textAlign: 'center',
    fontSize: 25,
    color: '#25E0E4',
    borderBottomWidth: 0.5,
    borderColor: '#25E0E4',
  },
  userInfoBody: {
    marginTop: 10,
    width: '90%',
  },
  userInfoBodyContainer: {
    alignItems: 'center',
  },
  userInfoText: {
    color: 'white',
    letterSpacing: 1,

    fontSize: 15,
  },
  userInfoEditWrapper: {
    width: '100%',
    height: 20,
  },
  editButtonWrapper: {
    flex: 1,
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  avatarStyle: {
    borderWidth: 1,
    padding: 2,
    borderColor: '#25E0E4',
  },
};

export default styles;
