import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import EIcon from 'react-native-vector-icons/FontAwesome';
import {Avatar} from 'react-native-elements';
import styles from './styles.js';

const UserCard = props => {
  return (
    <View style={styles.userCardContainer}>
      <View style={styles.userInfoHeaderContainer}>
        <View style={styles.userInfoHeader}>
          <View style={styles.userImage}>
            <Avatar
              containerStyle={styles.avatarStyle}
              rounded
              source={{
                uri: props.user && props.user.attributes.image_url,
              }}
              size="large"
            />
          </View>
          <View style={{width: '100%'}}>
            <Text style={styles.userName}>
              {props.user && props.user.attributes.name}
            </Text>
          </View>
        </View>
      </View>
      <View style={styles.userInfoBodyContainer}>
        <View style={styles.userInfoBody}>
          <Text style={styles.userInfoText}>
            {props.user && props.user.attributes.description}
          </Text>
          <View style={styles.userInfoEditWrapper}>
            <View style={styles.editButtonWrapper}>
              <TouchableOpacity onPress={props.handleClick}>
                <EIcon name={'edit'} size={16} style={{color: 'white'}} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default UserCard;
