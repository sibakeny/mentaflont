import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import axios from 'axios';
import {url} from '../../../shared/const';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as Actions from '../../../actions';
import styles from './styles.js';
import {ListItem} from 'react-native-elements';
import Header from '../../../shared/header';
import LinearGradient from 'react-native-linear-gradient';
import UserCard from '../../../shared/UserCard';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifications: [],
      image: '',
      selectedIndex: 0,
    };
  }
  componentDidMount() {
    if (this.props.auth.token == '') {
      this.props.navigation.navigate('Login');
    }
    this.props.getCurrentUser(this.props.auth.token);
    this.props.getNotification(this.props.auth.token);

    this.subs = [
      this.props.navigation.addListener('didFocus', () => this._onFocus()),
    ];
  }

  componentWillUnmount() {
    this.subs.forEach(sub => sub.remove());
  }

  _onFocus = () => {
    this.props.getCurrentUser(this.props.auth.token);
    this.props.getNotification(this.props.auth.token);
  };

  componentDidUpdate() {}

  navigateToMentarDetail = id => {
    this.props.navigation.navigate('MentarDetail', {
      mentar_id: id,
    });
  };

  navigateToPostConsultation = () => {
    this.props.navigation.navigate('PostConsultation');
  };

  navigateToConsultationHome = () => {
    this.props.navigation.navigate('ConsultationHome');
  };

  navigateToEditUserDescription = () => {
    this.props.navigation.navigate('EditUserDescription');
  };

  navigateToUserConsultations = () => {
    this.props.navigation.navigate('UserConsultations');
  };

  notificationAction = notification => {
    if (notification.category == 'directMessage') {
      this.goDirectMessage(notification.id, notification.sender.id);
    } else if (notification.category == 'Mentar') {
      // user_idしかないため移動出来ない
      // target_idのようにする必要あり
    }
  };

  notificationTypeComment = notification => {
    if (notification.attributes.category == 'directMessage') {
      return 'さんからの新着メッセージ';
    } else if (notification.attributes.category == 'Mentar') {
      return 'さんからの評価コメント';
    } else if (notification.attributes.category == 'Consultation') {
      return 'さんからの相談コメント';
    }
  };

  notificationNavigation = notification => {
    // delete notification
    axios.delete(url + '/notifications/' + notification.attributes.id, {});

    if (
      notification.attributes.opponent_type &&
      notification.attributes.opponent_type === 'Consultation'
    ) {
      this.showConsultationDetail(notification.attributes.opponent_id);
    } else if (
      notification.attributes.opponent_type &&
      notification.attributes.opponent_type === 'Mentar'
    ) {
      this.navigateToMentarDetail(notification.attributes.opponent_id);
    } else {
      this.goDirectMessage(
        notification.attributes.id,
        notification.relationships.sender.data.id,
      );
    }
  };

  renderNotifications = () => {
    return (
      this.props.notification &&
      this.props.notification.notifications &&
      this.props.notification.notifications.map(notification => {
        return (
          <TouchableOpacity
            style={styles.menuItem}
            key={notification.id}
            onPress={() => this.notificationNavigation(notification)}>
            <Text style={styles.itemText}>
              {notification.attributes.sender_name +
                this.notificationTypeComment(notification)}
            </Text>
          </TouchableOpacity>
        );
      })
    );
  };

  mentarListItem = () => {
    if (
      this.props.user.currentUser &&
      this.props.user.currentUser.relationships.mentar.data
    ) {
      return (
        <TouchableOpacity
          style={styles.menuItem}
          onPress={() => this.navigateToConsultationHome()}>
          <Text style={styles.itemText}>個人相談マイページ</Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          style={styles.menuItem}
          onPress={this.navigateToRegistarToMentar}>
          <Text style={styles.itemText}>メンターとして登録する</Text>
        </TouchableOpacity>
      );
    }
  };

  postConsultationListItem = () => {
    this.props.notification.notifications.map(item => console.log(item));
    const notificationCount = this.props.notification.notifications.filter(
      item => item.attributes.category == 'Consultation',
    ).length;

    console.log(this.props.notification.notifications);

    if (notificationCount > 0) {
      return (
        <ListItem
          title={'投稿した相談'}
          leftIcon={{name: 'book'}}
          onPress={this.navigateToUserConsultations}
          containerStyle={styles.listItem}
          titleStyle={{color: 'white'}}
          badge={{
            value: notificationCount,
            textStyle: {color: 'white'},
            status: 'success',
          }}
          chevron
        />
      );
    } else {
      return (
        <ListItem
          title={'投稿した相談'}
          leftIcon={{name: 'book'}}
          onPress={this.navigateToUserConsultations}
          containerStyle={styles.listItem}
          titleStyle={{color: 'white'}}
          chevron
        />
      );
    }
  };

  render() {
    return (
      <LinearGradient
        style={styles.flex}
        colors={['rgba(12, 19, 74, 1)', '#D22AD4']}
        start={{x: 1, y: 0}}
        end={{x: 1, y: 1}}>
        <Header navigation={this.props.navigation} title={'MentarSupport'} />
        <View>
          <View style={{marginHorizontal: 9}}>
            <UserCard
              handleClick={this.navigateToEditUserDescription}
              user={this.props.user.currentUser}
              name={
                this.props.user.currentUser &&
                this.props.user.currentUser.attributes.name
              }
            />
          </View>

          <View style={styles.homeList}>
            <ListItem
              title={'相談を投稿する'}
              leftIcon={{name: 'add'}}
              onPress={this.navigateToPostConsultation}
              containerStyle={styles.listItem}
              titleStyle={{color: 'white'}}
              chevron
            />
            <ListItem
              title={'個人相談マイページ'}
              leftIcon={{name: 'person'}}
              onPress={this.navigateToConsultationHome}
              containerStyle={styles.listItem}
              titleStyle={{color: 'white'}}
              chevron
            />
            {this.postConsultationListItem()}
          </View>
        </View>
        <View style={{marginBottom: 50}}></View>
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
