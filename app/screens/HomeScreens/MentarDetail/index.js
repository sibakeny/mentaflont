import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  View,
  TouchableOpacity,
  Text,
  ScrollView,
  Image,
  FlatList,
} from 'react-native';
import {bindActionCreators} from 'redux';
import * as Actions from '../../../actions';
import axios from 'axios';
import {url} from '../../../shared/const';
import styles from './styles.js';
import {ListItem, Avatar} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import Header from '../../../shared/header';
import UserCard from '../../../shared/UserCard';

class MentarDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mentar: null,
      comment: '',
      evaluationComments: [],
      loading: false,
      page: 0,
      stopLoading: false,
    };
  }

  componentDidMount = async () => {
    await axios
      .get(url + '/mentars/' + this.props.navigation.state.params.mentar_id, {
        params: {
          page: 1,
        },
      })
      .then(res => {
        this.setState({mentar: res.data.data});
      });

    await axios
      .get(url + '/comments', {
        params: {
          target_type: 'Mentar',
          target_id: this.props.navigation.state.params.mentar_id,
          page: 0,
        },
      })
      .then(res => {
        this.setState({evaluationComments: res.data.data});
      });

    this.subs = [
      this.props.navigation.addListener('didFocus', () => this._onFocus()),
    ];
  };

  componentWillUnmount() {
    this.subs.forEach(sub => sub.remove());
  }

  _onFocus = () => {
    axios
      .get(url + '/mentars/' + this.props.navigation.state.params.mentar_id, {
        params: {
          page: 1,
        },
      })
      .then(res => {
        this.setState({mentar: res.data.data});
      });

    axios
      .get(url + '/comments', {
        params: {
          target_type: 'Mentar',
          target_id: this.props.navigation.state.params.mentar_id,
          page: 0,
        },
      })
      .then(res => {
        this.setState({evaluationComments: res.data.data});
      });
  };

  goDirectMessage = () => {
    this.props.navigation.navigate('DirectMessage', {
      reciever_id: this.state.mentar.relationships.user.data.id,
    });
  };

  handlePost = () => {
    this.props.navigation.navigate('PostMentarEvaluationComment', {
      mentar: this.state.mentar,
    });
  };

  loadMoreItems() {
    axios
      .get(url + '/comments', {
        params: {
          target_type: 'Mentar',
          target_id: this.props.navigation.state.params.mentar_id,
          page: this.state.page + 1,
        },
      })
      .then(res => {
        var comments = this.state.evaluationComments;
        this.setState({page: this.state.page + 1});
        this.setState({loading: false});

        if (res.data.data.length > 0) {
          this.setState({stopLoading: false});
          this.setState({evaluationComments: comments.concat(res.data.data)});
        } else {
          this.setState({stopLoading: true});
        }
      });
  }

  renderComments() {
    return this.state.evaluationComments.map(comment => {
      return (
        <ListItem
          key={comment.attributes.id}
          leftAvatar={{source: {uri: comment.attributes.image_url}}}
          title={comment.attributes.user_name}
          subtitle={comment.attributes.body}
          subtitleStyle={{color: 'white'}}
          containerStyle={{
            marginHorizontal: 15,
            backgroundColor: 'transparent',
            shadowOffset: {
              width: 0,
              height: 8,
            },
            shadowOpacity: 0.46,
            shadowRadius: 11.14,

            elevation: 5,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          titleStyle={{color: 'white'}}
        />
      );
    });
  }

  isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  render() {
    console.log(this.state.mentar);
    return (
      <LinearGradient
        style={{flex: 1}}
        colors={['rgba(12, 19, 74, 1)', '#D22AD4']}
        start={{x: 1, y: 0}}
        end={{x: 1, y: 1}}>
        <ScrollView
          onScroll={({nativeEvent}) => {
            if (this.isCloseToBottom(nativeEvent)) {
              if (!this.state.loading && !this.state.stopLoading) {
                this.setState({loading: true});
                this.loadMoreItems();
              }
            }
          }}
          scrollEventThrottle={400}>
          <Header
            navigation={this.props.navigation}
            title={'MentarSupport'}
            child={true}
          />
          <View style={styles.container}>
            <View style={{marginHorizontal: 9}}>
              <UserCard
                user={this.state.mentar}
                name={this.props.user.currentUser.attributes.name}
              />
            </View>

            <View style={styles.ButtonContainer}>
              <TouchableOpacity
                style={styles.sendButton}
                onPress={this.goDirectMessage.bind(this)}>
                <Text style={styles.sendButtonText}>メッセージを送る</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.mentarsTitle}>
              <Text style={styles.mentarsTitleText}>評価</Text>
            </View>
            <View style={styles.postInputGroup}>
              <TouchableOpacity
                onPress={this.handlePost}
                style={styles.commentButton}>
                <Text style={styles.commentButtonText}>投稿</Text>
              </TouchableOpacity>
            </View>
            <View>{this.renderComments()}</View>
          </View>
        </ScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(MentarDetail);
