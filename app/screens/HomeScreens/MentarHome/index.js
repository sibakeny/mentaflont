import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as Actions from '../../../actions';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import {globalStyles} from '../../../styles/global';
import EIcon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';
import {url} from '../../../shared/const';
import Header from '../../../shared/header';
import styles from './styles';
import {ListItem, Avatar} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';

class MentarHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mentar: null,
      notifications: [],
    };
  }

  componentDidMount() {
    axios
      .get(url + '/users/' + this.props.auth.token + '/mentar')
      .then(response => {
        this.setState({mentar: response.data});
      });
    this.subs = [
      this.props.navigation.addListener('didFocus', () => this._onFocus()),
    ];
  }

  componentWillUnmount() {
    this.subs.forEach(sub => sub.remove());
  }

  _onFocus = () => {
    axios
      .get(url + '/users/' + this.props.auth.token + '/mentar')
      .then(response => {
        this.setState({mentar: response.data});
      });
  };

  navigateToRegisterToMentar = () => {
    this.props.navigation.navigate('RegistarToMentar', {});
  };

  navigateToEditMentar = () => {
    this.props.navigation.navigate('EditMentar', {});
  };

  navigateToMentarDetail = () => {
    this.props.navigation.navigate('MentarDetail', {
      mentar: this.state.mentar,
    });
  };

  categoryList = () => {
    return [
      {
        title: 'カテゴリ',
        icon: 'add',
        handleClick:
          this.state.mentar &&
          this.state.mentar.data &&
          this.state.mentar.data.attributes.category,
      },
      {
        title: '性別',
        icon: 'person',
        handleClick:
          this.state.mentar &&
          this.state.mentar.data &&
          this.state.mentar.data.attributes.sex,
      },
    ];
  };

  userInfoContainer = () => {
    if (
      this.state.mentar &&
      this.state.mentar.data &&
      this.state.mentar.data.attributes.description != ''
    ) {
      return (
        <View
          style={{
            paddingBottom: 20,
            paddingTop: 10,
            margin: 0,
            backgroundColor: "backgroundColor: 'rgba(39, 21, 110, 0.1)'",
            borderWidth: 0,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 8,
            },
            shadowOpacity: 0.46,
            shadowRadius: 11.14,

            elevation: 5,
          }}>
          <View style={styles.userInfoHeaderContainer}>
            <View style={styles.userInfoHeader}>
              <View style={styles.userImage}>
                <Avatar
                  containerStyle={styles.avatarStyle}
                  rounded
                  source={{
                    uri:
                      this.props.user.currentUser &&
                      this.props.user.currentUser.attributes.image_url,
                  }}
                  size="large"
                />
              </View>
              <View style={{width: '100%'}}>
                <Text style={styles.userName}>
                  {this.props.user.currentUser &&
                    this.props.user.currentUser.attributes.name}
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.userInfoBodyContainer}>
            <View style={styles.userInfoBody}>
              <Text style={styles.userInfoText}>
                {this.state.mentar &&
                  this.state.mentar.data &&
                  this.state.mentar.data.attributes.description}
              </Text>
              <View style={styles.userInfoEditWrapper}>
                <View style={styles.editButtonWrapper}>
                  <TouchableOpacity onPress={this.navigateToEditMentar}>
                    <EIcon name={'edit'} size={16} style={{color: 'white'}} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
      );
    } else if (
      this.state.mentar &&
      this.state.mentar.data &&
      this.state.mentar.data.attributes.description == ''
    ) {
      return (
        <View style={styles.suggestRegisterContainer}>
          <View style={styles.suggestRegisterWrap}>
            <Text style={{color: 'white'}}>
              まだメンターとして登録されていません。
            </Text>
            <Text style={{color: 'white'}}>
              まずはメンター登録をお願い致します。
            </Text>
            <TouchableOpacity
              style={styles.registerButton}
              onPress={this.navigateToRegisterToMentar}>
              <Text style={styles.registerText}>メンターとして登録する。</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  };

  goDirectMessage = (notification_id, user_id) => {
    // delete notification
    axios.delete(url + '/delete_direct_message_notifications', {
      params: {
        id: notification_id,
      },
    });

    this.props.navigation.navigate('DirectMessage', {
      reciever_id: user_id,
    });
  };

  renderNotifications = () => {
    return this.state.notifications.map(notification => {
      return (
        <TouchableOpacity
          style={styles.menuItem}
          key={notification.id}
          onPress={() =>
            this.goDirectMessage(notification.id, notification.user.id)
          }>
          <Text style={styles.itemText}>
            {notification.user.name}さんからの新着メッセージ
          </Text>
        </TouchableOpacity>
      );
    });
  };

  displaySex = () => {
    if (
      this.state.mentar &&
      this.state.mentar.data &&
      this.state.mentar.data.attributes.sex === 1
    ) {
      return '男性';
    } else if (
      this.state.mentar &&
      this.state.mentar.data &&
      this.state.mentar.data.attributes.sex === 2
    ) {
      return '女性';
    } else {
      return '';
    }
  };

  handlePost = () => {
    this.props.navigation.navigate('PostMentarEvaluationComment', {
      mentar: this.state.mentar,
    });
  };

  comments = () => {
    return (
      this.state.mentar &&
      this.state.mentar.included &&
      this.state.mentar.included.map(comment => {
        return (
          <ListItem
            key={comment.attributes.id}
            leftAvatar={{source: {uri: comment.attributes.image_url}}}
            title={comment.attributes.user_name}
            titleStyle={{color: 'white'}}
            subtitleStyle={{color: 'white'}}
            subtitle={comment.attributes.body}
            containerStyle={{
              backgroundColor: 'transparent',
              shadowOffset: {
                width: 0,
                height: 8,
              },
              shadowOpacity: 0.46,
              shadowRadius: 11.14,

              elevation: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          />
        );
      })
    );
  };

  render() {
    return (
      <LinearGradient
        style={{flex: 1}}
        colors={['rgba(12, 19, 74, 1)', '#D22AD4']}
        start={{x: 1, y: 0}}
        end={{x: 1, y: 1}}>
        <Header navigation={this.props.navigation} child={true} />
        <ScrollView>
          <View>
            <View style={styles.userInfoContainer}>
              {this.userInfoContainer()}
            </View>

            <View style={styles.evaluationTitle}>
              <Text style={styles.evaluationTitleText}>評価コメント一覧</Text>
            </View>
            <View style={styles.postInputGroup}>
              <TouchableOpacity
                onPress={this.handlePost}
                style={styles.commentButton}>
                <Text style={styles.commentButtonText}>投稿</Text>
              </TouchableOpacity>
            </View>
            <View>{this.comments()}</View>
          </View>
        </ScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(MentarHome);
