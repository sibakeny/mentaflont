import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  userInfoHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 70,
    position: 'relative',
    width: '90%',
    paddingVertical: 10,
  },
  userInfoHeaderContainer: {
    alignItems: 'center',
    paddingVertical: 10,
  },
  userImage: {},
  userName: {
    width: '80%',
    textAlign: 'center',
    fontSize: 25,
    color: 'white',
    borderBottomWidth: 0.5,
    borderColor: '#25E0E4',
  },
  userInfoBody: {
    marginTop: 10,
    width: '90%',
  },
  userInfoBodyContainer: {
    alignItems: 'center',
  },
  userInfoText: {
    color: 'white',
    fontSize: 15,
  },

  categoryTitleContainer: {
    marginTop: 40,
  },
  categoryTitleText: {
    fontSize: 20,
    color: 'white',
    left: 4,
  },
  selectFieldContainer: {},
  selectButtonContainer: {},
  blueButtonStyle: {
    height: 60,
    width: 130,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4DB4F9',
    borderRadius: 6,
    elevation: 3,
    shadowOffset: {width: 1, height: 1},
    shadowColor: '#333',
    shadowOpacity: 0.3,
    shadowRadius: 2,
  },
  orangeButtonStyle: {
    height: 60,
    width: 130,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FCA800',
    borderRadius: 6,
    elevation: 3,
    shadowOffset: {width: 1, height: 1},
    shadowColor: '#333',
    shadowOpacity: 0.3,
    shadowRadius: 2,
  },
  firstBlockContainer: {
    marginTop: 40,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  buttonText: {
    color: 'white',
    letterSpacing: 1,
    fontSize: 16,
  },
  userInfoEditWrapper: {
    width: '100%',
    height: 20,
  },
  menuListContainer: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    marginTop: 40,
    backgroundColor: 'white',
  },
  menuItem: {
    borderBottomWidth: 1,
    borderColor: '#707070',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 12,
  },
  itemText: {},
  latestInfo: {
    borderWidth: 1,
    borderColor: 'red',
    backgroundColor: 'white',
  },
  latestInfoNoItem: {
    borderWidth: 1,
    borderColor: 'gray',
    backgroundColor: 'white',
  },
  latestInfoText: {
    paddingVertical: 20,
  },
  moveMentarDetailButton: {
    width: '100%',
  },
  moveMentarDetailText: {
    color: 'white',
  },
  buttonWrapper: {
    marginTop: 20,
    marginHorizontal: 15,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarStyle: {
    borderWidth: 1,
    padding: 2,
    borderColor: '#25E0E4',
  },
  editButtonWrapper: {
    flex: 1,
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  evaluationTitle: {
    marginHorizontal: 15,
    marginVertical: 15,
    fontSize: 18,
    fontWeight: 'bold',
  },
  evaluationTitleText: {
    fontSize: 16,
    fontWeight: 'bold',
    borderBottomWidth: 1,
    borderColor: '#707070',
    paddingBottom: 4,
    color: 'white',
  },
  postInputGroup: {
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  commentButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 30,
    width: 80,
    borderRadius: 6,
    elevation: 3,
    backgroundColor: '#14B7FF',
    shadowOffset: {width: 1, height: 1},
    shadowColor: '#333',
    shadowOpacity: 0.3,
    shadowRadius: 2,
  },
  commentButtonText: {
    color: 'white',
  },
});

export default styles;
