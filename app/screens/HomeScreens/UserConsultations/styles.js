const styles = {
  container: {
    alignItems: 'center',
    flex: 1,
  },
  picker: {
    width: '100%',
    borderWidth: 1,
    borderColor: '#707070',
  },
  inputBoxGroup: {
    marginVertical: 5,
  },
  searchTextInput: {
    height: 40,
    borderWidth: 1,
    borderColor: '#707070',
    marginTop: 10,
    padding: 4,
    color: 'white',
  },
  mentarsContainer: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchInputWrapper: {
    marginHorizontal: 15,
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    height: 50,
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'white',
  },
  avatarStyle: {
    marginRight: 5,
  },
};

export default styles;
