import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, ScrollView} from 'react-native';
import {bindActionCreators} from 'redux';
import * as Actions from '../../../actions';
import axios from 'axios';
import {url} from '../../../shared/const';
import styles from './styles';
import LinearGradient from 'react-native-linear-gradient';
import Header from '../../../shared/header';
import {Badge} from 'react-native-elements';
import ConsultationCard from './components/ConsultationCard';

class SearchConsultation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      consultations: [],
      keyword: '',
      sex: '',
      category: '',
      orderVisible: false,
      filterCategoryVisible: false,
    };
  }

  componentDidMount() {
    axios
      .get(url + '/users/consultations', {
        params: {
          token: this.props.auth.token,
        },
      })
      .then(res => {
        this.setState({consultations: res.data.data});
      });

    this.subs = [
      this.props.navigation.addListener('didFocus', () => this._onFocus()),
    ];
  }

  _onFocus = () => {
    axios
      .get(url + '/users/consultations', {
        params: {
          token: this.props.auth.token,
        },
      })
      .then(res => {
        this.setState({consultations: res.data.data});
      });
  };

  showConsultationDetail = async id => {
    await axios.delete(url + '/notifications/all', {
      params: {
        category: 'Consultation',
        opponent_id: id,
        token: this.props.auth.token,
      },
    });

    this.props.navigation.navigate('UserConsultation', {
      consultation_id: id,
    });
  };

  mentarCategoryBadge = category => {
    if (category != '') {
      return <Badge value={category} status="success" />;
    }
  };

  mentarSexBadge = sex => {
    if (sex != '') {
      if (sex == '1') {
        return <Badge value={'男性'} status="success" />;
      } else if (sex == '2') {
        return <Badge value={'女性'} status="success" />;
      }
    }
  };

  consultations = () => {
    return this.state.consultations.map(consultation => {
      return (
        <ConsultationCard
          key={consultation.attributes.id}
          consultation={consultation}
          handleClick={this.showConsultationDetail}
          notificationCount={
            this.props.notification.notifications.filter(
              item =>
                item.attributes.opponent_id == consultation.attributes.id &&
                item.attributes.category == 'Consultation',
            ).length
          }
        />
      );
    });
  };

  render() {
    return (
      <LinearGradient
        style={{flex: 1}}
        colors={['rgba(12, 19, 74, 1)', '#D22AD4']}
        start={{x: 1, y: 0}}
        end={{x: 1, y: 1}}>
        <ScrollView>
          <Header navigation={this.props.navigation} child={true} />
          <View style={styles.container}>
            <View style={styles.mentarsContainer}>{this.consultations()}</View>
          </View>
          <View style={{marginBottom: 50}}></View>
        </ScrollView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchConsultation);
