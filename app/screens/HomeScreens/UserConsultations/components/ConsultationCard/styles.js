import {StyleSheet} from 'react-native';

const styles = {
  mentarCard: {
    flex: 1,
    width: '100%',
    borderRadius: 6,
    marginTop: 20,

    justifyContent: 'center',
    alignItems: 'center',
  },
  userInfoHeader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    minHeight: 50,
    position: 'relative',
    width: '100%',
    borderBottomWidth: 1,
    borderColor: '#25E0E4',
  },
  userImage: {
    width: 40,
    height: 40,
    borderRadius: 50,
    position: 'absolute',
    left: 0,
  },
  userName: {
    fontWeight: 'bold',
    fontSize: 20,
    color: 'white',
  },
  userInfoBody: {
    width: '100%',
    paddingVertical: 4,
  },
  userNameContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexDirection: 'row',
  },
  userInfoImage: {
    width: 30,
    height: 30,
    borderRadius: 50,
    marginRight: 10,
  },
  userInfoName: {
    fontSize: 17,
    color: 'white',
  },
  avatarStyle: {
    marginRight: 5,
  },
  badgeContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
  mentarCardWrapper: {
    paddingBottom: 20,
    paddingTop: 20,
    paddingHorizontal: 20,
    width: '95%',
    margin: 0,
    backgroundColor: "backgroundColor: 'rgba(39, 21, 110, 0.1)'",
    borderWidth: 0,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.46,
    shadowRadius: 11.14,

    elevation: 5,
  },
  textWhite: {
    color: 'white',
  },
  badgeTopContainer: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: 10,
  },
};

export default styles;
