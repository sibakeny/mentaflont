import React from 'react';
import {connect} from 'react-redux';
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Picker,
  TouchableOpacity,
} from 'react-native';
import {bindActionCreators} from 'redux';
import * as Actions from '../../../actions';
import axios from 'axios';
import {url} from '../../../shared/const';
import styles from './styles';

class RegistarToMentar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {description: '', category: '', sex: ''};
  }

  componentDidMount() {}

  navigateToMentarDetail = () => {
    this.props.navigation.navigate('MentarDetail', {});
  };

  handleSubmit = () => {
    axios
      .post(url + '/users/' + this.props.auth.token, '/mentars', {
        description: this.state.description,
        category: this.state.category,
        sex: this.state.sex,
      })
      .then(() => {
        this.props.navigation.navigate('ConsultationHome', {});
      });
  };

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.wrapAll}>
            <View style={styles.consultationTitle}>
              <Text style={styles.consultationTitleText}>
                メンターとして登録
              </Text>
            </View>
            <View style={styles.postCard}>
              <View style={styles.postCardWrap}>
                <View style={styles.selectBoxGroup}>
                  <Text>カテゴリ</Text>
                  <Picker
                    style={styles.picker}
                    onValueChange={itemValue =>
                      this.setState({category: itemValue})
                    }
                    selectedValue={this.state.category}>
                    <Picker.Item value="" label="指定なし" />
                    <Picker.Item value="仕事" label="仕事" />
                    <Picker.Item value="趣味" label="趣味" />
                    <Picker.Item value="お金" label="お金" />
                    <Picker.Item value="勉強" label="勉強" />
                  </Picker>
                </View>
                <View style={styles.selectBoxGroup}>
                  <Text>性別</Text>
                  <Picker
                    style={styles.picker}
                    onValueChange={itemValue => this.setState({sex: itemValue})}
                    selectedValue={this.state.sex}>
                    <Picker.Item value="0" label="指定なし" />
                    <Picker.Item value="1" label="男" />
                    <Picker.Item value="2" label="女" />
                  </Picker>
                </View>
                <View style={styles.inputBoxGroup}>
                  <Text>本文</Text>
                  <TextInput
                    multiline={true}
                    style={styles.searchTextArea}
                    onChangeText={val => this.setState({description: val})}
                  />
                </View>
                <View style={styles.submitButtonGroup}>
                  <TouchableOpacity
                    style={styles.submitButton}
                    onPress={this.handleSubmit.bind(this)}>
                    <Text style={styles.submitButtonText}>登録</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(RegistarToMentar);
