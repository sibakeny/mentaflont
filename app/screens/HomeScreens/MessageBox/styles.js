import {StyleSheet} from 'react-native';

const styles = {
  container: {
    alignItems: 'center',
  },
  messageContainer: {
    flex: 1,
    width: '100%',
    backgroundColor: '#fff',
    paddingVertical: 8,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: 'gray',
  },
  mentarsTitle: {
    width: '90%',
    marginVertical: 5,
  },
  mentarsTitleText: {
    fontSize: 16,
  },
  MessageContainer: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 4,
  },
  userInfoHeader: {
    height: 50,
    marginHorizontal: 15,
  },
  userImage: {
    width: 40,
    height: 40,
    borderRadius: 50,
  },
  userName: {
    fontWeight: 'bold',
    fontSize: 15,
    color: 'gray',
  },
  userInfoBody: {
    width: '90%',
  },
};

export default styles;
