import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, TouchableOpacity, ScrollView} from 'react-native';
import {bindActionCreators} from 'redux';
import * as Actions from '../../../actions';
import axios from 'axios';
import {url} from '../../../shared/const';
import {ListItem, Badge} from 'react-native-elements';
import Header from '../../../shared/header';
import LinearGradient from 'react-native-linear-gradient';

class MessageBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
    };
  }

  componentDidMount() {
    axios
      .get(url + '/users/' + this.props.auth.token + '/messaged_users')
      .then(res => {
        this.setState({users: res.data});
      });

    this.subs = [
      this.props.navigation.addListener('didFocus', () => this._onFocus()),
    ];
  }

  componentWillUnmount() {
    this.subs.forEach(sub => sub.remove());
  }

  _onFocus = () => {
    axios
      .get(url + '/users/' + this.props.auth.token + '/messaged_users')
      .then(res => {
        this.setState({users: res.data});
      });
  };

  goDirectMessage = id => {
    this.props.navigation.navigate('DirectMessage', {
      reciever_id: id,
    });
  };

  UserList = () => {
    var list = [];
    this.state.users.data &&
      this.state.users.data.map(user => {
        list.push({
          id: user.attributes.id,
          name: user.attributes.name,
          avatar_url: user.attributes.image_url,
          subtitle: user.attributes.latest_comment,
          notificationCount: user.attributes.notification_count,
        });
      });
    return list;
  };

  notificationBadge = count => {
    if (count == 0) {
      return null;
    } else {
      return (
        <View
          style={{
            width: '100%',
            position: 'absolute',
            justifyContent: 'flex-start',
            flexDirection: 'row',
          }}>
          <Badge
            containerStyle={{
              top: 10,
              right: -10,
            }}
            value={count}
            status="success"
          />
        </View>
      );
    }
  };

  renderItem = item => (
    <TouchableOpacity
      key={item.id}
      onPress={() => this.goDirectMessage(item.id)}>
      <ListItem
        containerStyle={{
          backgroundColor: 'rgba(2, 27, 28, 0.4)',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        title={item.name}
        titleStyle={{color: 'white'}}
        subtitle={item.subtitle}
        subtitleStyle={{color: 'white'}}
        leftAvatar={{source: {uri: item.avatar_url}}}
        chevron
      />
      {this.notificationBadge(item.notificationCount)}
    </TouchableOpacity>
  );

  render() {
    return (
      <LinearGradient
        style={{flex: 1}}
        colors={['rgba(12, 19, 74, 1)', '#D22AD4']}
        start={{x: 1, y: 0}}
        end={{x: 1, y: 1}}>
        <View style={{flex: 1}}>
          <Header navigation={this.props.navigation} title={'MentarSupport'} />
          <ScrollView>
            <View style={{marginTop: 10}}>
              {this.UserList().map(item => {
                return this.renderItem(item);
              })}
            </View>
          </ScrollView>
        </View>
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageBox);
