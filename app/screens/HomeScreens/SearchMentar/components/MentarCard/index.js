import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Avatar, Badge} from 'react-native-elements';
import styles from './styles.js';

const MentarCard = props => {
  const mentarCategoryBadge = mentar => {
    if (mentar.attributes.category != '') {
      return <Badge value={mentar.attributes.category} status="success" />;
    }
  };

  const mentarSexBadge = mentar => {
    if (mentar.attributes.sex != '') {
      if (mentar.attributes.sex == '1') {
        return <Badge value={'男性'} status="success" />;
      } else {
        return <Badge value={'女性'} status="success" />;
      }
    }
  };
  return (
    <TouchableOpacity
      style={styles.mentarCard}
      onPress={() => props.handleClick(props.mentar)}
      key={props.mentar.id}>
      <View style={styles.mentarCardWrapper}>
        <View style={styles.userInfoHeaderContainer}>
          <View style={styles.userInfoHeader}>
            <View style={styles.userImage}>
              <Avatar
                containerStyle={styles.avatarStyle}
                rounded
                source={{
                  uri: props.mentar.attributes.user_image_url,
                }}
                size="large"
              />
            </View>
            <View style={styles.maxWidth}>
              <Text style={styles.userName}>
                {props.mentar.attributes.user_name}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.userInfoBodyContainer}>
          <View style={styles.userInfoBody}>
            <Text style={styles.userInfoText}>
              {props.mentar.attributes.description}
            </Text>
            <View style={styles.badgeContainer}>
              <View style={{marginRight: 5}}>
                {mentarCategoryBadge(props.mentar)}
              </View>
              <View>{mentarSexBadge(props.mentar)}</View>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default MentarCard;
