import {StyleSheet} from 'react-native';

const styles = {
  container: {
    alignItems: 'center',
  },

  picker: {
    width: '100%',
    borderWidth: 1,
    borderColor: '#707070',
  },
  inputBoxGroup: {
    marginVertical: 5,
  },
  searchTextInput: {
    height: 40,
    borderWidth: 1,
    borderColor: '#707070',
    marginTop: 10,
    padding: 4,
  },

  floatingWrapper: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
  },
  floatingWrapperSecond: {
    width: 200,
    borderRadius: 25,
    shadowColor: '#ccc',
    backgroundColor: 'white',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 0,
    shadowOpacity: 1,
    elevation: 2,
    height: 40,
    opacity: 0.8,
    flex: 1,
    flexDirection: 'row',
  },
  floatingItemFirst: {
    width: '50%',
    borderRightWidth: 1,
    borderColor: 'gray',
    justifyContent: 'center',
    alignItems: 'center',
  },
  floatingItemSecond: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchInputWrapper: {
    marginHorizontal: 15,
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    height: 50,
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'white',
  },
  flex: {
    flex: 1,
  },
  searchInput: {width: '90%'},
  textWhite: {color: 'white'},
};

export default styles;
