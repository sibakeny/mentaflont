import React, {useState} from 'react';
import {View, Text, TouchableOpacity, Picker} from 'react-native';
import {Overlay, CheckBox, ListItem} from 'react-native-elements';
import styles from './styles.js';

const FloatingButton = props => {
  const [orderVisible, setOrderVisible] = useState(false);
  const [filterCategoryVisible, setFilterCategoryVisible] = useState(false);
  const [category, setCategory] = useState('');
  const [sex, setSex] = useState('');
  const [order, setOrder] = useState('');
  const [column, setColumn] = useState('');

  const categoryFilterBackdropClicked = () => {
    setFilterCategoryVisible(false);
    props.handleFilterOrSort(column, order, category, sex);
  };

  const orderBackdropClicked = () => {
    setOrderVisible(false);
    console.log(column);
    console.log(order);
    props.handleFilterOrSort(column, order, category, sex);
  };

  return (
    <View>
      <View style={styles.floatingWrapper}>
        <View style={styles.floatingWrapperSecond}>
          <TouchableOpacity
            onPress={() => setOrderVisible(true)}
            style={styles.floatingItemFirst}>
            <Text>並び替え</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setFilterCategoryVisible(true)}
            style={styles.floatingItemSecond}>
            <Text>絞り込み</Text>
          </TouchableOpacity>
        </View>
      </View>
      <Overlay
        isVisible={filterCategoryVisible}
        onBackdropPress={() => categoryFilterBackdropClicked()}
        width="auto"
        height="auto">
        <View style={{width: 300}}>
          <Picker
            style={styles.picker}
            onValueChange={itemValue => setCategory(itemValue)}
            selectedValue={category}>
            <Picker.Item value="" label="カテゴリ(指定なし)" />
            <Picker.Item value="仕事" label="仕事" />
            <Picker.Item value="趣味" label="趣味" />
            <Picker.Item value="お金" label="お金" />
            <Picker.Item value="勉強" label="勉強" />
          </Picker>
        </View>
        <View>
          <Picker
            style={styles.picker}
            onValueChange={itemValue => setSex(itemValue)}
            selectedValue={sex}>
            <Picker.Item value="0" label="性別(指定なし)" />
            <Picker.Item value="1" label="男" />
            <Picker.Item value="2" label="女" />
          </Picker>
        </View>
      </Overlay>
      <Overlay
        isVisible={orderVisible}
        onBackdropPress={() => orderBackdropClicked()}
        width="auto"
        height="auto">
        <View style={{width: 300}}>
          <ListItem
            title={'新着投稿順'}
            leftIcon={{name: 'history'}}
            onPress={() => {
              setOrder('desc');
              setColumn('created_at');
              orderBackdropClicked();
            }}
            bottomDivider
          />
          <ListItem
            title={'コメント数順'}
            leftIcon={{name: 'comment'}}
            onPress={() => {
              setOrder('desc');
              setColumn('comments');
              orderBackdropClicked();
            }}
          />
        </View>
      </Overlay>
    </View>
  );
};

export default FloatingButton;
