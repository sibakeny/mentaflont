import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, TextInput, ScrollView} from 'react-native';
import {bindActionCreators} from 'redux';
import * as Actions from '../../../actions';
import axios from 'axios';
import {url} from '../../../shared/const';
import styles from './styles';
import EIcon from 'react-native-vector-icons/FontAwesome';
import Header from '../../../shared/header';
import LinearGradient from 'react-native-linear-gradient';
import ConsultationCard from '../../../shared/TouchableUserCard';
import FloatingButton from './components/FloatingButton';

class SearchDirectSupport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mentars: [],
      keyword: '',
      orderCreatedAt: false,
      orderReview: false,
      sort: '',
      order: '',
    };
  }

  componentDidMount() {
    axios.get(url + '/mentars').then(res => {
      this.setState({mentars: res.data});
    });
    this.subs = [
      this.props.navigation.addListener('didFocus', () => this._onFocus()),
    ];
  }

  componentWillUnmount() {
    this.subs.forEach(sub => sub.remove());
  }

  _onFocus = () => {
    axios.get(url + '/mentars').then(res => {
      this.setState({mentars: res.data});
    });
  };

  moveToMentarDetail = mentar => {
    this.props.navigation.navigate('MentarDetail', {
      mentar_id: mentar.id,
    });
  };

  mentarsList = () => {
    return (
      this.state.mentars.data &&
      this.state.mentars.data.map(mentar => {
        return (
          <ConsultationCard
            mentar={mentar}
            handleClick={this.moveToMentarDetail}
          />
        );
      })
    );
  };

  handleSearch = () => {
    axios
      .get(url + '/mentars/search', {
        params: {
          keyword: this.state.keyword,
          orderCreatedAt: this.state.orderCreatedAt,
          orderReview: this.state.orderReview,
        },
      })
      .then(res => {
        this.setState({mentars: res.data});
      });
  };

  handleFilterOrSort = (column = '', order = '', category = '', sex = '') => {
    axios
      .get(url + '/mentars/sort', {
        params: {
          keyword: this.state.keyword,
          category: category,
          sex: sex,
          column: column,
          order: order,
        },
      })
      .then(res => {
        this.setState({mentars: res.data});
      });
  };

  render() {
    return (
      <LinearGradient
        style={styles.flex}
        colors={['rgba(12, 19, 74, 1)', '#D22AD4']}
        start={{x: 1, y: 0}}
        end={{x: 1, y: 1}}>
        <Header navigation={this.props.navigation} />
        <View style={styles.flex}>
          <ScrollView>
            <View style={styles.container}>
              <View style={styles.searchInputWrapper}>
                <EIcon
                  name="search"
                  style={{paddingHorizontal: 10, color: 'white'}}
                />
                <View style={styles.searchInput}>
                  <TextInput
                    style={styles.textWhite}
                    placeholderTextColor={'gray'}
                    placeholder="検索ワードを入力してください"
                    onSubmitEditing={this.handleSearch}
                    onChangeText={val => this.setState({keyword: val})}
                  />
                </View>
              </View>
              <View style={styles.mentarsContainer}>{this.mentarsList()}</View>
            </View>
            <View style={{marginBottom: 50}}></View>
          </ScrollView>
          <FloatingButton handleFilterOrSort={this.handleFilterOrSort} />
        </View>
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchDirectSupport);
