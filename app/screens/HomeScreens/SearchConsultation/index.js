import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, TextInput, ScrollView} from 'react-native';
import {bindActionCreators} from 'redux';
import * as Actions from '../../../actions';
import axios from 'axios';
import {url} from '../../../shared/const';
import styles from './styles';
import LinearGradient from 'react-native-linear-gradient';
import Header from '../../../shared/header';
import EIcon from 'react-native-vector-icons/FontAwesome';
import {Badge} from 'react-native-elements';
import ConsultationCard from '../../../shared/TouchableConsultationCard';
import FloatingButton from './components/FloatingButton';

class SearchConsultation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      consultations: [],
      keyword: '',
      sex: '',
      category: '',
      orderVisible: false,
      filterCategoryVisible: false,
    };
  }

  componentDidMount() {
    axios.get(url + '/consultations').then(res => {
      this.setState({consultations: res.data.data});
    });

    this.subs = [
      this.props.navigation.addListener('didFocus', () => this._onFocus()),
    ];
  }

  componentWillUnmount() {
    this.subs.forEach(sub => sub.remove());
  }

  _onFocus = () => {
    axios.get(url + '/consultations').then(res => {
      this.setState({consultations: res.data.data});
    });
  };

  showConsultationDetail = id => {
    this.props.navigation.navigate('ConsultationDetail', {
      consultation_id: id,
    });
  };

  mentarCategoryBadge = category => {
    if (category != '') {
      return <Badge value={category} status="success" />;
    }
  };

  mentarSexBadge = sex => {
    if (sex != '') {
      if (sex == '1') {
        return <Badge value={'男性'} status="success" />;
      } else {
        return <Badge value={'女性'} status="success" />;
      }
    }
  };

  consultations = () => {
    return this.state.consultations.map(consultation => {
      return (
        <ConsultationCard
          key={consultation.attributes.id}
          consultation={consultation}
          handleClick={this.showConsultationDetail}
        />
      );
    });
  };

  categoryFilterBackdropClicked = () => {
    this.setState({filterCategoryVisible: false});
    this.handleSearch();
  };

  orderBackdropClicked = () => {
    this.setState({orderVisible: false});
    this.handleSearch();
  };

  handleSearch = () => {
    axios
      .get(url + '/consultations/search', {
        params: {
          keyword: this.state.keyword,
        },
      })
      .then(res => {
        this.setState({consultations: res.data.data});
      });
  };

  handleFilterOrSort = (column = '', order = '', category = '', sex = '') => {
    axios
      .get(url + '/consultations/sort', {
        params: {
          keyword: this.state.keyword,
          category: category,
          sex: sex,
          column: column,
          order: order,
        },
      })
      .then(res => {
        this.setState({consultations: res.data.data});
      });
  };

  render() {
    return (
      <LinearGradient
        style={{flex: 1}}
        colors={['rgba(12, 19, 74, 1)', '#D22AD4']}
        start={{x: 1, y: 0}}
        end={{x: 1, y: 1}}>
        <ScrollView>
          <Header navigation={this.props.navigation} />
          <View style={styles.container}>
            <View style={styles.searchInputWrapper}>
              <EIcon
                name="search"
                style={{paddingHorizontal: 10, color: 'white'}}
              />
              <View style={{width: '90%'}}>
                <TextInput
                  style={{color: 'white'}}
                  placeholderTextColor={'gray'}
                  placeholder="検索ワードを入力してください"
                  onSubmitEditing={this.handleSearch}
                  onChangeText={val => this.setState({keyword: val})}
                />
              </View>
            </View>
            <View style={styles.mentarsContainer}>{this.consultations()}</View>
          </View>
          <View style={{marginBottom: 50}}></View>
        </ScrollView>
        <FloatingButton handleFilterOrSort={this.handleFilterOrSort} />
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchConsultation);
