import React, {useState, Component} from 'react';
import Navigator from './routes/drawer';
import {Provider} from 'react-redux';
import {View, YellowBox} from 'react-native';
import store, {persistor} from './store';
import {PersistGate} from 'redux-persist/integration/react';

export default class App extends Component {
  componentDidMount() {
    YellowBox.ignoreWarnings(['Setting a timer']);
  }
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <View style={{flex: 1, backgroundColor: 'red'}}>
            <Navigator />
          </View>
        </PersistGate>
      </Provider>
    );
  }
}
