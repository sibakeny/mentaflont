import React, {Component} from 'react';
import {Text} from 'react-native';
import {connect} from 'react-redux';
import {Badge} from 'react-native-elements';
import {bindActionCreators} from 'redux';
import * as Actions from '../../actions';

class NotiTabBarIcon extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    if (
      this.props.notification.notifications.filter(
        item => item.attributes.category == 'directMessage',
      ).length > 0
    ) {
      return (
        <Badge
          containerStyle={{position: 'absolute', top: -2, right: -20}}
          value={
            this.props.notification.notifications.filter(
              item => item.attributes.category == 'directMessage',
            ).length
          }
          status="success"
        />
      );
    } else {
      return null;
    }
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(NotiTabBarIcon);
