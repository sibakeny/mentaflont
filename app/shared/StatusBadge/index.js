import React from 'react';
import {Text, View} from 'react-native';

const StatusBadge = props => {
  if (props.status == 'open') {
    return (
      <View
        style={{
          borderColor: '#55C319',
          backgroundColor: '#55C319',
          borderWidth: 1,
          paddingHorizontal: 12,
          paddingVertical: 4,
        }}>
        <Text style={{color: 'white', fontSize: 10}}>公開中</Text>
      </View>
    );
  } else if (props.status == 'pending') {
    return (
      <View
        style={{
          borderColor: '#FBAD15',
          backgroundColor: '#FBAD15',
          borderWidth: 1,
          paddingHorizontal: 12,
          paddingVertical: 4,
        }}>
        <Text style={{color: 'white', fontSize: 10}}>一時停止中</Text>
      </View>
    );
  } else if (props.status == 'close') {
    return (
      <View
        style={{
          borderColor: '#FE190C',
          backgroundColor: '#FE190C',
          borderWidth: 1,
          paddingHorizontal: 12,
          paddingVertical: 4,
        }}>
        <Text style={{color: 'white', fontSize: 10}}>終了</Text>
      </View>
    );
  } else if (props.status == 'resolved') {
    return (
      <View
        style={{
          borderColor: '#rgb(33,137,217)',
          backgroundColor: '#rgb(33,137,217)',
          borderWidth: 1,
          paddingHorizontal: 12,
          paddingVertical: 4,
        }}>
        <Text style={{color: 'white', fontSize: 10}}>解決</Text>
      </View>
    );
  } else {
    return null;
  }
};

export default StatusBadge;
