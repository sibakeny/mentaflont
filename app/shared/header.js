import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import EIcon from 'react-native-vector-icons/FontAwesome';

export default function Header(props) {
  const headerIcon = () => {
    if (props.child === true) {
      return (
        <TouchableOpacity
          onPress={() => {
            props.navigation.goBack(null);
          }}>
          <EIcon
            name={'arrow-left'}
            style={{color: 'white', padding: 15}}
            size={20}
          />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity onPress={props.navigation.openDrawer}>
          <EIcon
            name={'navicon'}
            style={{color: 'white', padding: 15}}
            size={20}
          />
        </TouchableOpacity>
      );
    }
  };
  return (
    <View
      style={{
        marginTop: 5,
        marginHorizontal: 10,
        height: 50,
        backgroundColor: 'transparent',
        justifyContent: 'space-between',
        flexDirection: 'row',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 8,
        },
        shadowOpacity: 0.46,
        shadowRadius: 11.14,

        elevation: 17,
      }}>
      <TouchableOpacity onPress={props.navigation.openDrawer}>
        {headerIcon()}
      </TouchableOpacity>
      <View
        style={{
          width: '100%',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            paddingLeft: 90,
            fontSize: 18,
            fontWeight: 'bold',
            color: '#25E0E4',
            letterSpacing: 1,
          }}>
          MentaSupport
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {},
  headerText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#53F3E5',
    letterSpacing: 1,
  },
  headerImage: {
    width: 26,
    height: 26,
    marginHorizontal: 10,
  },
  headerTitle: {
    flexDirection: 'row',
  },
});
