const styles = {
  mentarCard: {
    marginHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  userInfoHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 70,
    position: 'relative',
    width: '100%',
  },
  userInfoHeaderContainer: {
    alignItems: 'center',
  },
  userName: {
    width: '75%',
    textAlign: 'center',
    fontSize: 20,
    color: '#25E0E4',
    borderBottomWidth: 0.5,
    borderColor: '#25E0E4',
  },
  userInfoBody: {
    marginTop: 10,
    marginBottom: 10,
    width: '90%',
  },
  userInfoBodyContainer: {
    alignItems: 'center',
  },
  userInfoText: {
    color: 'white',
    fontSize: 13,
  },
  avatarStyle: {
    borderWidth: 1,
    padding: 2,
    borderColor: '#25E0E4',
  },
  mentarCardWrapper: {
    width: '95%',
    paddingBottom: 20,
    paddingTop: 20,
    paddingHorizontal: 20,
    margin: 0,
    backgroundColor: "backgroundColor: 'rgba(39, 21, 110, 0.1)'",
    borderWidth: 0,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.46,
    shadowRadius: 11.14,

    elevation: 5,
  },
  badgeContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  maxWidth: {width: '100%'},
};

export default styles;
