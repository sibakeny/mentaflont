import React from 'react';
import {ListItem} from 'react-native-elements';

const CommentCard = props => {
  return (
    <ListItem
      key={props.comment.attributes.id}
      leftAvatar={{source: {uri: props.comment.attributes.image_url}}}
      title={props.comment.attributes.user_name}
      titleStyle={{
        color: 'white',
      }}
      subtitle={props.comment.attributes.body}
      subtitleStyle={{color: 'white'}}
      containerStyle={{
        backgroundColor: 'transparent',
        shadowOffset: {
          width: 0,
          height: 8,
        },
        shadowOpacity: 0.46,
        shadowRadius: 11.14,

        elevation: 5,
        justifyContent: 'center',
        alignItems: 'center',
      }}
      titleStyle={{color: 'white'}}
    />
  );
};

export default CommentCard;
