import firebase from 'firebase';
import {url} from './shared/const';
import axios from 'axios';

//==========================================================================
//------------------------------ 認証関係------------------------------------
//==========================================================================

export const changeEmail = text => {
  return {
    type: 'change_email',
    payload: text,
  };
};

export const changePassword = text => {
  return {
    type: 'change_password',
    payload: text,
  };
};

export const submitLogin = ({email, password}) => {
  return dispatch => {
    axios
      .post(url + '/sessions', {
        params: {
          email: email,
          password: password,
        },
      })
      .then(res => {
        dispatch({type: 'logined', payload: res.data.data.attributes.token});
      })
      .catch(err => console.log(err));
  };
};

export const submitLogout = () => {
  firebase.auth().signOut();
  return {
    type: 'logout_success',
  };
};

export const logout = () => {
  return {
    type: 'logout',
  };
};

//==========================================================================
//------------------------------ ユーザー情報-------------------------------
//==========================================================================

export const getCurrentUser = token => {
  return dispatch => {
    axios
      .get(url + '/users/current_user', {
        params: {
          token: token,
        },
      })
      .then(response => {
        dispatch({
          type: 'SET_CURRENT_USER',
          payload: response.data.data,
        });
      });
  };
};

//==========================================================================
//------------------------------  通知関係 ---------------------------------
//==========================================================================

export const getNotification = token => {
  return dispatch => {
    axios
      .get(url + '/notifications', {
        params: {
          token: token,
        },
      })
      .then(res => {
        dispatch({type: 'SET_NOTIFICATION', payload: res.data.data});
      });
  };
};
