const INITIAL_STATE = {
  notifications: [],
};

const UserReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'SET_NOTIFICATION':
      return {...state, notifications: action.payload};
    default:
      return state;
  }
};

export default UserReducer;
