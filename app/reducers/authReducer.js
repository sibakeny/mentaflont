const INITIAL_STATE = {
  token: '',
};

const AuthReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'logined':
      return {...state, token: action.payload};
    case 'logout':
      return {...state, token: ''};
    default:
      return state;
  }
};

export default AuthReducer;
