import {combineReducers} from 'redux';
import AuthReducer from './authReducer';
import UserReducer from './userReducer';
import NotificationReducer from './notificationReducer';

const appReducer = combineReducers({
  auth: AuthReducer,
  user: UserReducer,
  notification: NotificationReducer,
});

const rootReducer = (state, action) => {
  if (action.type === 'logout') {
    state = undefined;
  }
  return appReducer(state, action);
};

export default rootReducer;
